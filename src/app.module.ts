import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TasksController } from './tasks/tasks.controller';
import { TasksService } from './tasks/tasks.service';
import { TasksModule } from './tasks/tasks.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [TasksModule,MongooseModule.forRoot('mongodb+srv://ian:1234@cluster0.1z6vy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
    useNewUrlParser:true
  })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
